import React from 'react'
import ReactDOM from 'react-dom/client'
import { CssBaseline, ThemeProvider, createTheme } from '@mui/material'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import SignIn from './SignIn';
import SignUp from './SignUp';
import PhoneBook, {loader as phoneBookLoader} from './PhoneBook';

const router = createBrowserRouter([
  {
    path: "/",
    element: <SignIn/>,
  },
  {
    path: "/signIn",
    element: <SignIn/>,
  },
  {
    path: "/signUp",
    element: <SignUp/>,
  },
  {
    path: "/phoneBook/:userId",
    element: <PhoneBook />,
    loader: phoneBookLoader
  }
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ThemeProvider theme={createTheme()}>
      <CssBaseline />
      <RouterProvider router={router} />
    </ThemeProvider>
  </React.StrictMode>,
)
