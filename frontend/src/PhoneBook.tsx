import { Box, Container, Typography } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import axios from 'axios';
import { useLoaderData } from 'react-router-dom';

export async function loader({params} : {params: any}) {
  try {
      return (await axios.get(`http://127.0.0.1:8002/users/` + params.userId)).data.contacts
  } catch (error) {
      return {}
  }
}

export default function PhoneBook() {

  const contacts: any = useLoaderData()

  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 150 },
    { field: 'name', headerName: 'Name', width: 150 },
    { field: 'surname', headerName: 'Surname', width: 150 },
    { field: 'address', headerName: 'Address', width: 150 },
    { field: 'phone_number', headerName: 'Phone', width: 150 },
    { field: 'age', headerName: 'Age', width: 150 },
  ];

  return (
    <ThemeProvider theme={createTheme()}>
      <CssBaseline />
      <Container>
        <Typography variant="h1">
          Phone Book
        </Typography>
        <Box sx={{ height: 400, width: '100%' }}>
          <DataGrid rows={contacts} columns={columns} />
        </Box>
      </Container>
    </ThemeProvider>
  );
}
