from pydantic import (
    BaseModel,
    Field,
    PositiveInt,
    UUID1,
)


class ContactCreate(BaseModel):
    name: str = Field(min_length=1)
    surname: str = Field(min_length=1)
    address: str = Field(min_length=1)
    phone_number: str = Field(
        min_length=1,
        pattern="^(\\((00|\\+)39\\)|(00|\\+)39)?(38[890]|34[7-90]|36[680]|33[3-90]|32[89])\\d{7}$",
    )
    age: PositiveInt


class Contact(ContactCreate):
    id: UUID1 = Field(validation_alias="_id")

    class Config:
        from_attribute = True


class ContactModify(ContactCreate):
    pass


class UserBase(BaseModel):
    user_name: str = Field(min_length=1)


class UserCreate(UserBase):
    password: str = Field(min_length=1)


class User(UserBase):
    id: UUID1 = Field(validation_alias="_id")

    contacts: list[Contact] = []

    class Config:
        from_attribute = True
