import logging
from typing import Annotated
from fastapi import APIRouter, Body, Depends, HTTPException, Path
from pydantic import UUID1
from pymongo import MongoClient

from ..model import schemas
from . import crud
from ..database import database

router = APIRouter(
    prefix="/users",
    tags=["users"],
)

logger = logging.getLogger("uvicorn.error")


@router.get("/", response_model=list[schemas.User])
async def read_users(
    db: MongoClient = Depends(database.get_db),
):
    return crud.get_users(db=db)


@router.get("/{user_id}", response_model=schemas.User)
async def read_user(
    user_id: Annotated[UUID1, Path()],
    db: MongoClient = Depends(database.get_db),
):
    if db_user := crud.get_user_by_id(db=db, user_id=user_id):
        return db_user
    raise HTTPException(status_code=404, detail="User Not found")


@router.post("/login", response_model=schemas.User)
async def login(
    user: Annotated[schemas.UserCreate, Body()],
    db: MongoClient = Depends(database.get_db),
):
    logger.debug(user)
    user_dict: dict = crud.get_user_by_username(db=db, user_name=user.user_name)
    if not user_dict:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    db_user = schemas.UserCreate(**user_dict)
    if not db_user.password == user.password:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return user_dict


@router.post("/register", response_model=schemas.User)
async def register(
    user: Annotated[schemas.UserCreate, Body()],
    db: MongoClient = Depends(database.get_db),
):
    if crud.get_user_by_username(db=db, user_name=user.user_name):
        raise HTTPException(status_code=400, detail="Username already in use")
    return crud.create_user(db=db, user=user)
