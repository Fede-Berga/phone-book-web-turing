import logging
import uuid
import bcrypt
from pymongo import MongoClient
from pydantic import UUID1

from ..model import schemas
from ..database import database, models


logger = logging.getLogger("uvicorn.error")


def get_users(db: MongoClient) -> list[schemas.User]:
    return list(db[database.db_name][database.collection_name].find({}))


def get_user_by_id(db: MongoClient, user_id: UUID1) -> schemas.User:
    return db[database.db_name][database.collection_name].find_one({"_id": user_id})


def get_user_by_username(db: MongoClient, user_name: str) -> schemas.User:
    return db[database.db_name][database.collection_name].find_one(
        {"user_name": user_name}
    )


def create_user(db: MongoClient, user: schemas.UserCreate) -> schemas.User:
    #user.password = bcrypt.hashpw(bytes(user.password, "utf-8"), salt).decode("utf-8")

    db_user = models.DBUser(**user.model_dump())

    with db.start_session(causal_consistency=True) as session:
        with session.start_transaction():
            inserted_id = (
                db[database.db_name][database.collection_name]
                .insert_one({"_id": uuid.uuid1(), **db_user.model_dump()})
                .inserted_id
            )

        return get_user_by_id(db=db, user_id=inserted_id)
