from pydantic import BaseModel, Field, PositiveInt

class Contact(BaseModel):
    name: str = Field(min_length=1)
    surname: str = Field(min_length=1)
    address: str = Field(min_length=1)
    phone_number: str = Field(
        min_length=1,
        pattern="^(\\((00|\\+)39\\)|(00|\\+)39)?(38[890]|34[7-90]|36[680]|33[3-90]|32[89])\\d{7}$",
    )
    age: PositiveInt

class DBUser(BaseModel):
    user_name: str = Field(min_length=1)
    password: str = Field(min_length=1)
    contacts: list[Contact] = []

    class Config:
        from_attribute = True
