import logging
import os
from pymongo import MongoClient
from urllib.parse import quote_plus

logger = logging.getLogger("uvicorn.error")

db_name = os.environ["DB_NAME"]
collection_name = "phone_book"

DATABASE_URL = "".join(
    [
        "mongodb://",
        quote_plus(os.environ["DB_USER"]),
        ":",
        quote_plus(os.environ["DB_PASSWORD"]),
        "@",
        quote_plus(os.environ["DB_HOST"]),
        ":",
        quote_plus(os.environ["DB_PORT"]),
    ]
)

logger.debug(f"{DATABASE_URL}")


def get_db():
    return MongoClient(DATABASE_URL, uuidRepresentation="standard")
