from fastapi import APIRouter, Depends, HTTPException
from pydantic import UUID1
from pymongo import MongoClient

from ..database import database
from ..model import schemas
from . import crud as contacts_crud
from ..user import crud as user_crud


router = APIRouter(
    prefix="/users/{user_id}/contacts",
    tags=["contacts"],
)


@router.get("/", response_model=list[schemas.Contact])
async def read_contacts(user_id: UUID1, db: MongoClient = Depends(database.get_db)):
    if not user_crud.get_user_by_id(db, user_id=user_id):
        raise HTTPException(status_code=404, detail="The specified user does not exist")

    return contacts_crud.get_contacts_by_user_id(db=db, user_id=user_id)


@router.post("/", response_model=schemas.Contact)
async def create_contact(
    user_id: UUID1,
    contact: schemas.ContactCreate,
    db: MongoClient = Depends(database.get_db),
):
    if not user_crud.get_user_by_id(db, user_id=user_id):
        raise HTTPException(status_code=404, detail=f"User {user_id} does not exist")

    if contacts_crud.get_contact_for_specific_user(
        db=db, user_id=user_id, phone_number=contact.phone_number
    ):
        raise HTTPException(
            status_code=404,
            detail=f"Phone number {contact.phone_number} is already present for user_id {user_id}",
        )

    return contacts_crud.create_contact(db=db, user_id=user_id, contact=contact)


@router.put("/{contact_id}", response_model=schemas.Contact)
async def modify_contact(
    user_id: UUID1,
    contact_id: UUID1,
    contact: schemas.ContactModify,
    db: MongoClient = Depends(database.get_db),
):
    if not user_crud.get_user_by_id(db, user_id=user_id):
        raise HTTPException(status_code=404, detail=f"User {user_id} does not exist")

    if not contacts_crud.get_contact_for_specific_user(
        db=db, user_id=user_id, contact_id=contact_id
    ):
        raise HTTPException(
            status_code=404,
            detail=f"Contact_id {contact_id} is not present for user_id {user_id}",
        )

    return contacts_crud.modify_contact(
        db=db, user_id=user_id, contact_id=contact_id, contact=contact
    )


@router.delete("/{contact_id}", response_model=schemas.Contact)
async def modify_contact(
    user_id: UUID1,
    contact_id: UUID1,
    db: MongoClient = Depends(database.get_db),
):
    if not user_crud.get_user_by_id(db, user_id=user_id):
        raise HTTPException(status_code=404, detail=f"User {user_id} does not exist")

    if not contacts_crud.get_contact_for_specific_user(
        db=db, user_id=user_id, contact_id=contact_id
    ):
        raise HTTPException(
            status_code=404,
            detail=f"Contact_id {contact_id} is not present for user_id {user_id}",
        )

    return contacts_crud.delete_contact(db=db, user_id=user_id, contact_id=contact_id)
