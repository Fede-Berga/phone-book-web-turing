import logging
import uuid
from pydantic import UUID1
from pymongo import MongoClient
from pymongo.client_session import ClientSession

from ..database import models, database
from ..model import schemas


logger = logging.getLogger("uvicorn.error")

get_new_id = uuid.uuid1


def get_contacts_by_user_id(
    db: MongoClient, user_id: UUID1, session: ClientSession | None = None
) -> list[schemas.Contact]:
    return db[database.db_name][database.collection_name].find_one(
        {"_id": user_id}, {"contacts": True}, session=session
    )["contacts"]


def get_contact_for_specific_user(
    db: MongoClient,
    user_id: UUID1,
    phone_number: str | None = None,
    contact_id: UUID1 | None = None,
    session: ClientSession | None = None,
) -> schemas.Contact:
    assert (phone_number is None) ^ (
        contact_id is None
    ), "Exactly one of `phone_number` and `contact_id` must be provided"

    if result := db[database.db_name][database.collection_name].find_one(
        {
            "_id": user_id,
            "contacts": {
                "$elemMatch": (
                    {"phone_number": phone_number}
                    if phone_number is not None
                    else {"_id": contact_id}
                )
            },
        },
        session=session,
    ):
        return result["contacts"][0]
    else:
        return result


def create_contact(
    db: MongoClient, user_id: UUID1, contact: schemas.ContactCreate
) -> schemas.Contact:
    with db.start_session(causal_consistency=True) as session:

        db_contact = models.Contact(**contact.model_dump())

        db[database.db_name][database.collection_name].update_one(
            {"_id": user_id},
            {"$push": {"contacts": {"_id": get_new_id(), **db_contact.model_dump()}}},
            session=session,
        )

        return get_contact_for_specific_user(
            db, user_id, contact.phone_number, session=session
        )


def modify_contact(
    db: MongoClient, user_id: UUID1, contact_id: UUID1, contact: schemas.ContactModify
) -> schemas.Contact:
    with db.start_session(causal_consistency=True) as session:
        db[database.db_name][database.collection_name].update_one(
            {
                "_id": user_id,
                "contacts": {"$elemMatch": {"_id": contact_id}},
            },
            {"$set": {"contacts.$": {"_id": contact_id, **contact.model_dump()}}},
            session=session,
        )

        return get_contact_for_specific_user(
            db, user_id, contact_id=contact_id, session=session
        )


def delete_contact(
    db: MongoClient, user_id: UUID1, contact_id: UUID1
) -> schemas.Contact:
    with db.start_session(causal_consistency=True) as session:

        contact_to_be_deleted = get_contact_for_specific_user(
            db=db, user_id=user_id, contact_id=contact_id, session=session
        )

        db[database.db_name][database.collection_name].update_one(
            {
                "_id": user_id,
                "contacts": {"$elemMatch": {"_id": contact_id}},
            },
            {"$pull": {"contacts": {"_id": contact_id}}},
            session=session,
        )

        return contact_to_be_deleted
